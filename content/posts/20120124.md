---
title: "Personal Cloud HowTo"
date: 2012-01-24
tags: ["sysadmin", "unix", "freebsd", "qmail"]
---
Set up a personal cloud in less than 30 minutes!

## Box
I'm using a [Panix VPS](http://www.panix.com/corp/v-colo/vplans.html), which
lets me run FreeBSD (on Xen).

Once you've signed up, you need to pick an OS via
{{< highlight shell >}}V-Colo management -> OS Loader{{< /highlight >}}
when that's done, restart your machine from
{{< highlight shell >}}V-Colo management -> VPS Info -> Status{{< /highlight >}}
you can then find the public address of your instance under
{{< highlight shell >}}V-Colo management -> Tools -> IPv4 Tools{{< /highlight >}}

## DNS
[Gandi](https://www.gandi.net/) has been my naming provider of choice for
several years now. My zone file looks like so:

{{< highlight shell >}}
Name   Type Value        TTL
----------------------------
@      A    166.84.7.90  3h
bliki  A    166.84.7.90  3h
canned A    166.84.7.90  3h
lists  A    166.84.7.90  3h
mx     A    166.84.7.90  3h
planet A    166.84.7.90  3h
@      MX   mx (10)      3h
{{< /highlight >}}

## Basic FreeBSD Configuration
### Account
{{< highlight shell >}}# adduser{{< /highlight >}}
(Remember to add your new user to group **wheel**).

### Packages
{{< highlight shell >}}# pkg_add -r mg wget tmux autotools gmake portmaster git{{< /highlight >}}

### SSH
Disable remote root logins on the server by setting **PermitRootLogin** to **no**:
{{< highlight shell >}}# mg /etc/ssh/sshd_config{{< /highlight >}}

Enable password-less logins from your laptop/workstation:
{{< highlight shell >}}$ scp ~/.ssh/id_rsa.pub jakob@primat.es:/home/jakob/.ssh/authorized_keys{{< /highlight >}}

### Duo Security
{{< highlight shell >}}
# pkg_add -r duo
# mg /usr/local/etc/duo/login_duo.conf
# echo "ForceCommand /usr/local/sbin/login_duo" >> /etc/ssh/sshd_config
# /etc/rc.d/sshd restart
{{< /highlight >}}

See also: http://www.duosecurity.com/docs/duounix

## HTTP
### nginx
{{< highlight shell >}}
# pkg_add -r nginx
# rm /usr/local/etc/nginx/nginx.conf
# ln -s /home/jakob/www/nginx.conf /usr/local/etc/nginx/nginx.conf
# echo 'nginx_enable="YES"' >> /etc/rc.conf
# /usr/local/etc/rc.d/nginx start
{{< /highlight >}}

### Planet Venus
{{< highlight shell >}}
# portmaster textproc/py-libxml2
# portmaster textproc/py-libxslt
# portmaster databases/py-bsddb
# cd /opt
# git clone https://github.com/rubys/venus.git
# cd venus
# python runtests.py
$ echo "@hourly cd /home/jakob/www/planet && python /opt/venus/planet.py config.ini" >> ~/.crontab
$ crontab ~/.crontab
{{< /highlight >}}

## SMTP
### qmail

Install packages:

{{< highlight shell >}}
# pkg_add -r qmail
# pkg_add -r ucspi-tcp
{{< /highlight >}}

Configure qmail:

{{< highlight shell >}}
# echo primat.es > /var/qmail/locals
# echo primat.es > /var/qmail/me
# echo primat.es > /var/qmail/rcpthosts
{{< /highlight >}}

Default aliases:

{{< highlight shell >}}
# echo jakob > /var/qmail/alias/.qmail-root 
# echo jakob > /var/qmail/alias/.qmail-postmaster 
# echo jakob > /var/qmail/alias/.qmail-mailer-daemon 
{{< /highlight >}}

Configure qmail-smtpd:

{{< highlight shell >}}
# echo '127.0.0.1:allow,RELAYCLIENT=""' > /etc/tcp.smtp
# echo :allow >> /etc/tcp.smtp
{{< /highlight >}}

Configure user:

{{< highlight shell >}}
$ /var/qmail/bin/maildirmake ~/Maildir
$ echo "./Maildir/" > ~/.qmail
{{< /highlight >}}

Enable qmail:

{{< highlight shell >}}
# /var/qmail/scripts/enable-qmail
# echo qmailsmtpd_enable="YES" >> /etc/rc.conf
{{< /highlight >}}

Start qmail:

{{< highlight shell >}}
# /etc/rc.d/sendmail stop
# cp /var/qmail/boot/maildir /var/qmail/rc
# /usr/local/etc/rc.d/qmail.sh start
# ln -s /var/qmail/rc-smtpd /usr/local/rc.d/qmail-smtpd.sh
# cp /var/qmail/boot/qmail-smtpd.rcNG /var/qmail/rc-smtpd
# /usr/local/rc.d/qmail-smtp.sh start
{{< /highlight >}}

{{< figure src="/img/loathers.png" class="alignleft" >}}
